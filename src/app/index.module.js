(function() {
  'use strict';

  angular
    .module('dataVisualizationsAndAngularJs', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMaterial']);

})();
